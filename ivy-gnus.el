;;; ivy-gnus.el --- Access gnus groups or servers using ivy

;; Filename: ivy-gnus.el
;; Description: Access gnus groups or servers using ivy
;; Author: Joe Bloggs <vapniks@yahoo.com>
;;         HydraDarkNg
;; Maintainer: HydraDarkNg
;; Copyleft (Ↄ) 2013, HydraDarkNg
;; Created: 2021-07-18 15:23
;; Version: 1.0
;; Last-Updated: 2021-07.19 15:23:00
;;           By: HydraDarkNg
;; URL: https://gitlab.com/HydraDarkNg/ivy-gnus/
;; Keywords: comm
;; Compatibility: GNU Emacs 24.3.1
;; Package-Requires: ((gnus "5.13"))
;;
;; Features that might be required by this library:
;;
;; gnus ivy
;;

;;; This file is NOT part of GNU Emacs

;;; License
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.
;; If not, see <http://www.gnu.org/licenses/>.

;;; Commentary: 
;;
;; Bitcoin donations gratefully accepted: 16mhw12vvtzCwQyvvLAqCSBvf8i41CfKhK
;; LBC(Lbry Credits) donations gratefully accepted: bE28eXgkTiEBLpaJF9J4WgcYRbsn8nis68
;;

;;; Commands:
;;
;; Below is a complete command list:
;;
;;  `ivy-gnus-select-group'
;;    Select a gnus group to visit using ivy.
;;  `ivy-gnus-select-server'
;;    Select a gnus server to visit using ivy.
;;  `ivy-gnus-select'
;;    Select a gnus group/server or existing gnus buffer using ivy.
;;  `ivy-gnus-select-account'
;;    Select a gnus acounts or existing gnus buffer using ivy.
;;; Customizable Options:
;;
;; Below is a customizable option list:
;;
;;  `ivy-gnus-show-article'
;;    If non-nil then an article will be auto-selected on group entry.
;;    default = nil
;;  `ivy-gnus-num-articles'
;;    The number of articles to display when a group is visited.
;;    default = t
;;
;; All of the above can customized by:
;;      M-x customize-group RET ivy-gnus RET
;;


;;; Installation:
;;
;; Put ivy-gnus.el in a directory in your load-path, e.g. ~/.emacs.d/
;; You can add a directory to your load-path with the following line in ~/.emacs
;; (add-to-list 'load-path (expand-file-name "~/elisp"))
;; where ~/elisp is the directory you want to add 
;; (you don't need to do this for ~/.emacs.d - it's added by default).
;;
;; Add the following to your ~/.emacs.d/init.el startup file.
;;
;; (require 'ivy-gnus)



;;; Change log:
;;	
;; 2013/06/21
;;      * First released.
;; 

;;; Acknowledgements:
;;
;; 
;;

;;; TODO
;;
;; 
;;

;;; Require
(require 'gnus)
(require 'ivy)

;;; Code:
(defgroup ivy-gnus nil
  "Access to gnus groups using ivy."
  :group 'gnus
  :group 'ivy)

(defcustom ivy-gnus-show-article nil
  "If non-nil then an article will be auto-selected on group entry."
  :group 'ivy-gnus
  :type 'boolean)

(defcustom ivy-gnus-num-articles t
  "The number of articles to display when a group is visited.
If this is a positive number, fetch this number of the latest
articles in the group.  If it is a negative number, fetch this
number of the earliest articles in the group.
If it is not a number then prompt the user for the number of articles."
  :group 'ivy-gnus
  :type '(choice (const :tag "Prompt for number of articles." t)
                 (integer :tag "Number of articles")))

;;;###autoload
(defun ivy-gnus-select-group (prefix)
  "Select a gnus group to visit using ivy.
If a prefix arg is used then the sense of `ivy-gnus-num-articles' will be reversed:
  if it is a number then the number of articles to display will be prompted for,
otherwise `gnus-large-newsgroup' articles will be displayed.

gnus will be started if it is not already running."
  (interactive "P")
  (unless gnus-newsrc-alist (gnus))
  (let* ((groups (mapcar 'car (cdr gnus-newsrc-alist)))
         (group (ivy-completing-read "Group: " groups nil t)))
    (if (member group groups)
        (gnus-group-read-group (if prefix
                                   (if (numberp ivy-gnus-num-articles) t
                                     gnus-large-newsgroup)
                                 ivy-gnus-num-articles)
                               (not ivy-gnus-show-article) group))))

;;;###autoload
(defun ivy-gnus-select-server nil
  "Select a gnus server to visit using ivy.

gnus will be started if it is not already running."
  (interactive)
  (unless gnus-newsrc-alist (gnus))
  (unless (get-buffer gnus-server-buffer) (gnus-enter-server-buffer))
  (let* ((srvs (mapcar (lambda (x) (concat (symbol-name (caar x)) ":" (cadar x)))
                       gnus-opened-servers))
         (srv (ivy-completing-read "Server: " srvs nil t)))
    (if (member srv srvs)
        (with-current-buffer gnus-server-buffer
          (gnus-server-read-server srv)))))

;;;###autoload
(defun ivy-gnus-select (prefix)
  "Select a gnus group/server or existing gnus buffer using ivy."
  (interactive "P")
  (unless gnus-newsrc-alist (gnus))
  (let* ((gnusbuffers (loop for buf in (buffer-list)
                            for mode = (with-current-buffer buf major-mode)
                            if (memq mode (list 'gnus-article-mode 'gnus-summary-mode))
                            collect (buffer-name buf)))
         (items (append '("Group buffer" "Other groups" "Server buffer" "Other servers" "Account Buffer") gnusbuffers))
         (item (ivy-completing-read "Open: " items nil t)))
    (cond ((equal item "Other groups") (ivy-gnus-select-group prefix))
          ((equal item "Group buffer") (switch-to-buffer gnus-group-buffer))
          ((equal item "Other servers") (ivy-gnus-select-server))
	  ((equal item "Account Buffer") (ivy-gnus-select-account))
          ((equal item "Server buffer")
           (unless (get-buffer gnus-server-buffer) (gnus-enter-server-buffer))
           (switch-to-buffer gnus-server-buffer))
          (t (switch-to-buffer item)))))

(defun ivy-gnus-select-account nil 
  "Select acount nnimap form gnus-secondary-select-methods"
  (interactive)
  (unless gnus-newsrc-alist (gnus))
  (unless (get-buffer gnus-server-buffer) (gnus-enter-server-buffer))
  (let* ((acounts (mapcar 'cdr (car (list gnus-secondary-select-methods))))
	 (acount (ivy-completing-read "Account:" acounts nil t)))
    (with-current-buffer gnus-server-buffer
          (gnus-server-read-server (concat "nnimap:" acount)))))

(provide 'ivy-gnus)

;;; ivy-gnus.el ends here
